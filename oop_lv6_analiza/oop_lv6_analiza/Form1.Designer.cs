﻿namespace oop_lv6_analiza
{
    partial class moja_forma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.prvi_broj = new System.Windows.Forms.TextBox();
            this.calculate_button = new System.Windows.Forms.Button();
            this.drugi_broj = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.zbrajanje_rez = new System.Windows.Forms.Label();
            this.razlika_rez = new System.Windows.Forms.Label();
            this.umnozak_rez = new System.Windows.Forms.Label();
            this.kvocijent_rez = new System.Windows.Forms.Label();
            this.sin_prvog = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cos_prvog = new System.Windows.Forms.Label();
            this.log_prvog = new System.Windows.Forms.Label();
            this.sqrt_prvog = new System.Windows.Forms.Label();
            this.prvi_nakvadrat = new System.Windows.Forms.Label();
            this.drugi_nakvadrat = new System.Windows.Forms.Label();
            this.sqrt_drugog = new System.Windows.Forms.Label();
            this.log_drugog = new System.Windows.Forms.Label();
            this.cos_drugog = new System.Windows.Forms.Label();
            this.sin_drugog = new System.Windows.Forms.Label();
            this.izlaz = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // prvi_broj
            // 
            this.prvi_broj.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.prvi_broj.Location = new System.Drawing.Point(94, 76);
            this.prvi_broj.Name = "prvi_broj";
            this.prvi_broj.Size = new System.Drawing.Size(152, 26);
            this.prvi_broj.TabIndex = 0;
            // 
            // calculate_button
            // 
            this.calculate_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.calculate_button.Location = new System.Drawing.Point(252, 55);
            this.calculate_button.Name = "calculate_button";
            this.calculate_button.Size = new System.Drawing.Size(120, 59);
            this.calculate_button.TabIndex = 2;
            this.calculate_button.Text = "Racunaj";
            this.calculate_button.UseVisualStyleBackColor = true;
            this.calculate_button.Click += new System.EventHandler(this.calculate_button_Click);
            // 
            // drugi_broj
            // 
            this.drugi_broj.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drugi_broj.Location = new System.Drawing.Point(378, 76);
            this.drugi_broj.Name = "drugi_broj";
            this.drugi_broj.Size = new System.Drawing.Size(152, 26);
            this.drugi_broj.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Zbroj";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Razlika";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Umnozak";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 262);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Kvocijent";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(12, 297);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "sin X";
            // 
            // zbrajanje_rez
            // 
            this.zbrajanje_rez.AutoSize = true;
            this.zbrajanje_rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zbrajanje_rez.Location = new System.Drawing.Point(305, 161);
            this.zbrajanje_rez.Name = "zbrajanje_rez";
            this.zbrajanje_rez.Size = new System.Drawing.Size(15, 20);
            this.zbrajanje_rez.TabIndex = 9;
            this.zbrajanje_rez.Text = "-";
            // 
            // razlika_rez
            // 
            this.razlika_rez.AutoSize = true;
            this.razlika_rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.razlika_rez.Location = new System.Drawing.Point(305, 196);
            this.razlika_rez.Name = "razlika_rez";
            this.razlika_rez.Size = new System.Drawing.Size(15, 20);
            this.razlika_rez.TabIndex = 10;
            this.razlika_rez.Text = "-";
            // 
            // umnozak_rez
            // 
            this.umnozak_rez.AutoSize = true;
            this.umnozak_rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.umnozak_rez.Location = new System.Drawing.Point(305, 230);
            this.umnozak_rez.Name = "umnozak_rez";
            this.umnozak_rez.Size = new System.Drawing.Size(15, 20);
            this.umnozak_rez.TabIndex = 11;
            this.umnozak_rez.Text = "-";
            // 
            // kvocijent_rez
            // 
            this.kvocijent_rez.AutoSize = true;
            this.kvocijent_rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kvocijent_rez.Location = new System.Drawing.Point(305, 262);
            this.kvocijent_rez.Name = "kvocijent_rez";
            this.kvocijent_rez.Size = new System.Drawing.Size(15, 20);
            this.kvocijent_rez.TabIndex = 12;
            this.kvocijent_rez.Text = "-";
            // 
            // sin_prvog
            // 
            this.sin_prvog.AutoSize = true;
            this.sin_prvog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sin_prvog.Location = new System.Drawing.Point(115, 297);
            this.sin_prvog.Name = "sin_prvog";
            this.sin_prvog.Size = new System.Drawing.Size(15, 20);
            this.sin_prvog.TabIndex = 13;
            this.sin_prvog.Text = "-";
            this.sin_prvog.Click += new System.EventHandler(this.sin_prvog_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(12, 331);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "cos X";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(12, 365);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "log X";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(12, 397);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "sqrt X";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(12, 429);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "X ^ 2";
            // 
            // cos_prvog
            // 
            this.cos_prvog.AutoSize = true;
            this.cos_prvog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cos_prvog.Location = new System.Drawing.Point(115, 331);
            this.cos_prvog.Name = "cos_prvog";
            this.cos_prvog.Size = new System.Drawing.Size(15, 20);
            this.cos_prvog.TabIndex = 18;
            this.cos_prvog.Text = "-";
            // 
            // log_prvog
            // 
            this.log_prvog.AutoSize = true;
            this.log_prvog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.log_prvog.Location = new System.Drawing.Point(115, 365);
            this.log_prvog.Name = "log_prvog";
            this.log_prvog.Size = new System.Drawing.Size(15, 20);
            this.log_prvog.TabIndex = 19;
            this.log_prvog.Text = "-";
            // 
            // sqrt_prvog
            // 
            this.sqrt_prvog.AutoSize = true;
            this.sqrt_prvog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sqrt_prvog.Location = new System.Drawing.Point(115, 397);
            this.sqrt_prvog.Name = "sqrt_prvog";
            this.sqrt_prvog.Size = new System.Drawing.Size(15, 20);
            this.sqrt_prvog.TabIndex = 20;
            this.sqrt_prvog.Text = "-";
            // 
            // prvi_nakvadrat
            // 
            this.prvi_nakvadrat.AutoSize = true;
            this.prvi_nakvadrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.prvi_nakvadrat.Location = new System.Drawing.Point(115, 429);
            this.prvi_nakvadrat.Name = "prvi_nakvadrat";
            this.prvi_nakvadrat.Size = new System.Drawing.Size(15, 20);
            this.prvi_nakvadrat.TabIndex = 21;
            this.prvi_nakvadrat.Text = "-";
            // 
            // drugi_nakvadrat
            // 
            this.drugi_nakvadrat.AutoSize = true;
            this.drugi_nakvadrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drugi_nakvadrat.Location = new System.Drawing.Point(388, 429);
            this.drugi_nakvadrat.Name = "drugi_nakvadrat";
            this.drugi_nakvadrat.Size = new System.Drawing.Size(15, 20);
            this.drugi_nakvadrat.TabIndex = 26;
            this.drugi_nakvadrat.Text = "-";
            // 
            // sqrt_drugog
            // 
            this.sqrt_drugog.AutoSize = true;
            this.sqrt_drugog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sqrt_drugog.Location = new System.Drawing.Point(388, 397);
            this.sqrt_drugog.Name = "sqrt_drugog";
            this.sqrt_drugog.Size = new System.Drawing.Size(15, 20);
            this.sqrt_drugog.TabIndex = 25;
            this.sqrt_drugog.Text = "-";
            // 
            // log_drugog
            // 
            this.log_drugog.AutoSize = true;
            this.log_drugog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.log_drugog.Location = new System.Drawing.Point(388, 365);
            this.log_drugog.Name = "log_drugog";
            this.log_drugog.Size = new System.Drawing.Size(15, 20);
            this.log_drugog.TabIndex = 24;
            this.log_drugog.Text = "-";
            // 
            // cos_drugog
            // 
            this.cos_drugog.AutoSize = true;
            this.cos_drugog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cos_drugog.Location = new System.Drawing.Point(388, 331);
            this.cos_drugog.Name = "cos_drugog";
            this.cos_drugog.Size = new System.Drawing.Size(15, 20);
            this.cos_drugog.TabIndex = 23;
            this.cos_drugog.Text = "-";
            // 
            // sin_drugog
            // 
            this.sin_drugog.AutoSize = true;
            this.sin_drugog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sin_drugog.Location = new System.Drawing.Point(388, 297);
            this.sin_drugog.Name = "sin_drugog";
            this.sin_drugog.Size = new System.Drawing.Size(15, 20);
            this.sin_drugog.TabIndex = 22;
            this.sin_drugog.Text = "-";
            // 
            // izlaz
            // 
            this.izlaz.Location = new System.Drawing.Point(27, 468);
            this.izlaz.Name = "izlaz";
            this.izlaz.Size = new System.Drawing.Size(571, 38);
            this.izlaz.TabIndex = 27;
            this.izlaz.Text = "Izlaz";
            this.izlaz.UseVisualStyleBackColor = true;
            this.izlaz.Click += new System.EventHandler(this.izlaz_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(91, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 28;
            this.label10.Text = "Prvo broj";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(378, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 17);
            this.label11.TabIndex = 29;
            this.label11.Text = "Drugi broj";
            // 
            // moja_forma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 518);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.izlaz);
            this.Controls.Add(this.drugi_nakvadrat);
            this.Controls.Add(this.sqrt_drugog);
            this.Controls.Add(this.log_drugog);
            this.Controls.Add(this.cos_drugog);
            this.Controls.Add(this.sin_drugog);
            this.Controls.Add(this.prvi_nakvadrat);
            this.Controls.Add(this.sqrt_prvog);
            this.Controls.Add(this.log_prvog);
            this.Controls.Add(this.cos_prvog);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sin_prvog);
            this.Controls.Add(this.kvocijent_rez);
            this.Controls.Add(this.umnozak_rez);
            this.Controls.Add(this.razlika_rez);
            this.Controls.Add(this.zbrajanje_rez);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.drugi_broj);
            this.Controls.Add(this.calculate_button);
            this.Controls.Add(this.prvi_broj);
            this.Name = "moja_forma";
            this.Text = "Moj kalkic";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox prvi_broj;
        private System.Windows.Forms.Button calculate_button;
        private System.Windows.Forms.TextBox drugi_broj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label zbrajanje_rez;
        private System.Windows.Forms.Label razlika_rez;
        private System.Windows.Forms.Label umnozak_rez;
        private System.Windows.Forms.Label kvocijent_rez;
        private System.Windows.Forms.Label sin_prvog;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label cos_prvog;
        private System.Windows.Forms.Label log_prvog;
        private System.Windows.Forms.Label sqrt_prvog;
        private System.Windows.Forms.Label prvi_nakvadrat;
        private System.Windows.Forms.Label drugi_nakvadrat;
        private System.Windows.Forms.Label sqrt_drugog;
        private System.Windows.Forms.Label log_drugog;
        private System.Windows.Forms.Label cos_drugog;
        private System.Windows.Forms.Label sin_drugog;
        private System.Windows.Forms.Button izlaz;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

