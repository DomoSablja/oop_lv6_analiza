﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_lv6_analiza
{
    public partial class moja_forma : Form
    {
        public moja_forma()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void sin_prvog_Click(object sender, EventArgs e)
        {

        }

        private void izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void calculate_button_Click(object sender, EventArgs e)
        {
            double operand1, operand2;
            if (!double.TryParse(prvi_broj.Text, out operand1))     //provjeravamo da li unosimo broj
                MessageBox.Show("Pogrešan unos prvog operanta", "Error!");
            else if(!double.TryParse(drugi_broj.Text,out operand2))
                MessageBox.Show("Pogrešan unos drugog operanta", "Error!");
            else            //ostatak racunanja
            {
                zbrajanje_rez.Text = (operand1 + operand2).ToString();
                razlika_rez.Text = (operand1 - operand2).ToString();
                umnozak_rez.Text = (operand1 * operand2).ToString();
                if (operand2 != 0)
                {
                    kvocijent_rez.Text = (operand1 / operand2).ToString();
                }
                else
                {
                    MessageBox.Show("Nazivnik ne smije biti 0");
                    kvocijent_rez.Text = "NaN";
                }
                
                sin_prvog.Text = (Math.Sin(operand1)).ToString();
                sin_drugog.Text = (Math.Sin(operand2)).ToString();
                cos_prvog.Text = (Math.Cos(operand1)).ToString();
                cos_drugog.Text = (Math.Cos(operand2)).ToString();
                log_prvog.Text = (Math.Log10(operand1)).ToString();
                log_drugog.Text = (Math.Log10(operand2)).ToString();
                sqrt_prvog.Text = (Math.Sqrt(operand1)).ToString();
                sqrt_drugog.Text = (Math.Sqrt(operand2)).ToString();
                prvi_nakvadrat.Text = (Math.Pow(operand1, 2)).ToString();
                drugi_nakvadrat.Text = (Math.Pow(operand2, 2)).ToString();



            }
        }
    }
}
